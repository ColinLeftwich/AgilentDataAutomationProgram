import sys
import re
import dpt

from typing import Optional, Tuple

from dataclasses import dataclass, field

import xlwings as xw

@dataclass
class Cell:
    """Excel Cell."""
    letter: str
    number: int
    value: str = None

    def __post_init__(self):
        if self.value is None:
            self.value = f"{self.letter}{self.number}"

    def __str__(self) -> str: 
        return f"{self.letter}{self.number}"

    def update(self, cell) -> None:
        self.letter = cell[0]
        self.number = int(cell[1])
        self.value = f"{self.letter}{self.number}"
    
@dataclass
class Column:
    """Column Data"""
    title: str
    channel_number: int
    start_cell:  str

@dataclass
class Graph:
    """Graph data"""
    title: str
    columns: list[Column]

    def add_column(self, column):
        """Add Column to be graphed."""
        self.columns.append(column)

class AgilentTabularDataFile:
    """A class to store tabular data generated from Agilent Data Loggers."""

    def __init__(self, working_file, logger):
        """Create Class."""
        try:
            self.workbook = xw.Book(working_file)
            self.data_sheet = self.workbook.sheets[0]
            self.data_sheet.name = "Data"
            self.graph_sheet = None
            self.logger = logger

        except Exception as error:
            self.logger.critical(f"{error}")
            self.logger.exception(error)

    def create_graph_sheet(self):
        """Create Graph Sheet."""
        try:
            number_of_sheets = len(self.workbook.sheets) - 1
            self.graph_sheet = self.workbook.sheets.add(name="Graphs", after=self.workbook.sheets[number_of_sheets])

        except Exception as error:
            self.logger.critical(f"{error}")
            self.logger.exception(error)

    def create_min_max_sheet(self):
        """Create Graph Sheet."""
        try:
            number_of_sheets = len(self.workbook.sheets) - 1
            self.graph_sheet = self.workbook.sheets.add(name="Min Max", after=self.workbook.sheets[number_of_sheets])

        except Exception as error:
            self.logger.critical(f"{error}")
            self.logger.exception(error)


class NewAgilentFile(AgilentTabularDataFile):
    """A class to store tabular data generated from Agilent Data Loggers."""
    # Data start cell
    data_start_cell: Cell

    # Time start cell
    time_start_cell: Cell

    # Instrument Configuration start cell
    ic_start_cell: Cell

    # Channel Configuration
    cc_start_cell: Cell

    # Channel name start cell
    ch_name_start_cell: Cell

    # Channel number start cell
    ch_number_start_cell: Cell

    # Relay Information
    ri_start_cell: Optional[Cell]

    def __init__(self, working_file, logger):
        """Create Class."""
        super().__init__(working_file, logger)
        self.cell_cache()

    def cell_cache(self):
        """Calculates start cells values for constants."""
        start = self.data_sheet.range('A:A').value
        for index, value in enumerate(start):

            if value is None:
                continue

            if isinstance(value, str):
                if value == "Instrument Configuration":
                    start_cell = Cell('A', index + 1)
                    self.logger.info(f"Instrument Configuration: {start_cell}")
                    self.ic_start_cell = start_cell
                    continue

                if value == "Channel Configuration":
                    self.cc_start_cell = Cell('A', index + 1)
                    self.ch_number_start_cell = Cell('A', index + 3)
                    self.ch_name_start_cell = Cell('B', index + 3)
                    self.logger.info(f"Channel Configuration found: {self.cc_start_cell}")
                    self.logger.info(f"Channel Number's/Name's found: {self.ch_number_start_cell}/{self.ch_name_start_cell}")
                    continue

                if value == "Relay Information":
                    start_cell = Cell('A', index + 1)
                    self.logger.info(f"Relay Information found: {start_cell}")
                    self.ri_start_cell = start_cell
                    continue

                if value == "Scan Sweep Time (Sec)":
                    self.data_start_cell = Cell('C', index + 1)
                    self.time_start_cell = Cell('A', index + 1)
                    self.logger.info(f"Data Found: {start_cell}")
                    break

        if not hasattr(self, 'data_start_cell'):
            self.logger.critical("CAN'T FIND DATA")

        if not hasattr(self, 'time_start_cell'):
            self.logger.critical("COULDN'T FIND TIME")

        if not hasattr(self, 'ic_start_cell'):
            self.logger.warning("COULDN'T FIND INSTRUMENT CONFIGURATION")

        if not hasattr(self, 'cc_start_cell'):
            self.logger.critical("COULDN'T FIND CHANNEL CONFIGURATION")

        if not hasattr(self, 'ch_number_start_cell') or not hasattr(self, 'ch_name_start_cell'):
            self.logger.critial("COULDN'T FIND CHANNEL NAME AND/OR NUMBER")

        if not hasattr(self, 'ri_start_cell'):
            self.logger.warning("Couldn't find Relay Information")
            self.ri_start_cell = None


    def reprlog(self):
        """Log string representation."""
        self.logger.info(f"Data: {self.data_start_cell}")
        self.logger.info(f"Instrument Configuration: {self.ic_start_cell}")
        self.logger.info(f"Channel Configuration: {self.cc_start_cell}")


class OldAgilentFile(AgilentTabularDataFile):
    """A class to store tabular data generated from Agilent Data Loggers."""
    # Data start Cell
    data_start_cell: Cell

    # Time start cell
    time_start_cell: Cell

    # Instrument Configuration start cell
    ic_start_cell: Cell

    # Channel Configuration
    cc_start_cell: Cell

    # Channel name start cell
    ch_name_start_cell: Cell

    # Channel number start cell
    ch_number_start_cell: Cell


    def __init__(self, working_file, logger):
        """Create Class."""
        super().__init__(working_file, logger)
        self.data_start_cell = "A10"
        self.cell_cache()

    def cell_cache(self):
        """Calculates start cells values for constants."""
        start = self.data_sheet.range('A:A').value
        for index, value in enumerate(start):

            if value is None:
                continue

            if isinstance(value, str):
                if value == "&Instrument:":
                    self.ic_start_cell = Cell('A', index + 1)
                    self.logger.info(f"Instrument Configuration: {self.ic_start_cell}")
                    continue

                if value == "Total Channels:":
                    self.cc_start_cell = Cell('A', index + 1)
                    self.ch_number_start_cell = Cell('A', index + 3)
                    self.ch_name_start_cell = Cell('B', index + 3)
                    self.logger.info(f"Channel Configuration found: {self.cc_start_cell}")
                    continue

                if value == "Scan":
                    self.data_start_cell = Cell('A', index + 1)
                    self.time_start_cell = Cell('B', index + 1)
                    self.logger.info(f"Data Found: {self.data_start_cell}")
                    break

        if not hasattr(self, 'data_start_cell'):
            self.logger.critical("COULDN'T FIND DATA")
        
        if not hasattr(self, 'time_start_cell'):
            self.logger.critical("COULDN'T FIND TIME")

        if not hasattr(self, 'ic_start_cell'):
            self.logger.critical("COULDN'T FIND INSTRUMENT CONFIGURATION")

        if not hasattr(self, 'cc_start_cell'):
            self.logger.critical("COULDN'T FIND CHANNEL CONFIGURATION")

        if not hasattr(self, 'ch_number_start_cell') or not hasattr(self, 'ch_name_start_cell'):
            self.logger.critical("COULDN'T FIND CHANNEL NAME AND/OR NUMBER")


    def reprlog(self):
        """Log string representation."""
        self.logger.info(f"Data: {self.data_start_cell}")
        self.logger.info(f"Instrument Configuration: {self.ic_start_cell}")
        self.logger.info(f"Channel Configuration: {self.cc_start_cell}")