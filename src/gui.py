from io import StringIO
import datetime
import logging
import os
import sys

from PyQt5.QtGui import (
    QIcon,
    QColor,
)
from PyQt5.QtWidgets import (
    QMainWindow,
    QDialog,
    QFileDialog,
    QLabel,
    QTextEdit,
    QTextBrowser,
    QLineEdit,
    QPushButton,
    QListWidget,
    QVBoxLayout,
    QGridLayout,
    QCheckBox,
    QAction,
    QListWidgetItem
)

from PyQt5.QtCore import Qt

# TODO FIX IMPORT
from PyQt5.uic import loadUi

from agilent import Graph, Column
from lib import run


basedir: str = os.path.dirname(__file__)


class MainWindow(QMainWindow):
    """Main Window."""

    def __init__(self):
        """Constructor Method."""
        super().__init__()

        # Main Window setup
        loadUi(os.path.join(basedir, "UI", "MainWindow.ui"), self)
        self.setWindowTitle("Adap")
        self.setWindowIcon(QIcon(os.path.join(basedir, "UI\\icon", "adap.ico")))
        self.setWindowFlags(Qt.WindowStaysOnTopHint)

        # Grab Main Windows Objects
        self.chooseFileLabel = self.findChild(QLabel, "chooseFileLabel")
        self.browseButton = self.findChild(QPushButton, "browseButton")
        self.submitButton = self.findChild(QPushButton, "submitButton")
        self.cancelButton = self.findChild(QPushButton, "cancelButton")
        self.filepathLineEdit = self.findChild(QLineEdit, "filepathLineEdit")
        self.outputNameLineEdit = self.findChild(QLineEdit, "outputNameLineEdit")
        self.menuDbgConsole = self.findChild(QAction, "actionDbgConsole")
        self.menuAboutPage = self.findChild(QAction, "actionAbout")

        self.menuDbgConsole.triggered.connect(self.showDbgConsoleDialog)
        self.menuAboutPage.triggered.connect(self.showAboutDialog)

        self.browseButton.clicked.connect(self.browse)
        self.submitButton.clicked.connect(self.submit)

        # Setups Secondary Dialog Windows
        self.DbgConsole = DbgConsoleDialog()
        self.AboutPage = AboutDialog()
        self.GraphWindow = GraphWindowDialog()
        self.ui_reset = False

        self.show()

    def browse(self):
        """Open File Browser."""
        input_file: list[str, str] = QFileDialog.getOpenFileName(self, "Open File", "", "CSV Files (*.csv);;All Files(*)")

        if input_file:
            self.filepathLineEdit.setText(f"{input_file[0]}")

            if self.filepathLineEdit.text() != "":
                output_file_name: str = self.filepathLineEdit.text()
                output_file_name = output_file_name.rstrip(".csv")
                output_file_name = output_file_name + " Graph"
                self.outputNameLineEdit.setText(f"{output_file_name}")

            elif self.filepathLineEdit.text() == "":
                self.outputNameLineEdit.setText(f"{input_file[0]}")
                self.filepathLineEdit.setText(f"{input_file[0]}")

    def submit(self):
        """Submit files."""
        input_data_file: str = self.filepathLineEdit.text()
        filename: str = self.outputNameLineEdit.text()

        if self.ui_reset:
            self.cleanup()
            self.ui_reset = False

        logger = self.DbgConsole.logger
        self.DbgConsole.filename = filename
        logger.info(f" {filename}")

        if len(input_data_file) > 0:
            self.ui_reset = run(input_data_file, filename, logger, self.GraphWindow)

    def showDbgConsoleDialog(self):
        """Open Debug Console."""
        if self.DbgConsole.isVisible():
            self.DbgConsole.hide()

        else:
            self.DbgConsole.show()

    def showAboutDialog(self):
        """Open About Page."""
        if self.AboutPage.isVisible():
            self.AboutPage.hide()

        else:
            self.AboutPage.show()

    def cancel(self):
        """Stop Program."""
        # TODO FIND A WAY TO STOP PROCESS (THREADS???)
        self.outputNameLineEdit.setText("")
        self.filepathLineEdit.setText("")
        sys.exit()

    def cleanup(self):
        """Reset values"""
        self.DbgConsole.console.clear()
        self.DbgConsole.log_handler.log_file_buffer.close()
        self.DbgConsole.log_handler.log_file_buffer = StringIO()

        self.GraphWindow.GraphListWidget.clear()

        for checkbox in self.GraphWindow.checkboxes:
            checkbox.close()

        for attribute in self.GraphWindow.get_ds_attributes():
            attribute.clear()

class AboutDialog(QDialog):
    """About Page."""

    def __init__(self):
        """Constructor Method."""
        super().__init__()

        loadUi(os.path.join(basedir, "UI", "About.ui"), self)
        self.setWindowTitle("About")
        self.setWindowFlags(Qt.WindowStaysOnTopHint)

        self.description = self.findChild(QTextBrowser, "DescriptionTextBrowser")
        self.ok_button = self.findChild(QPushButton, "OkButton")

        self.description.setOpenExternalLinks(True) 
        self.ok_button.clicked.connect(self.ok_close)

    def ok_close(self):
        self.close()


class DbgConsoleDialog(QDialog):
    """Debug Console."""

    def __init__(self):
        """Constructor Method."""
        super().__init__()

        loadUi(os.path.join(basedir, "UI", "DbgConsole.ui"), self)
        self.setWindowTitle("Debug Console")
        self.setWindowIcon(QIcon(os.path.join(basedir, "UI\\icon", "terminal.ico")))
        self.setWindowFlags(Qt.WindowStaysOnTopHint)

        self.console = self.findChild(QTextEdit, "console")
        self.saveLogButton = self.findChild(QPushButton, "saveLogButton")
        self.console.setReadOnly(True)
        self.filename = ""

        self.saveLogButton.clicked.connect(self.save_log)

        # Setups logger
        self.log_handler = QTextEditLoggerHandler(self.console)
        self.logger = logging.getLogger('__name__')
        self.logger.setLevel(logging.DEBUG)
        self.logger.addHandler(self.log_handler)
        console_fmt = logging.Formatter('[%(levelname)s] - %(message)s')
        self.log_handler.setFormatter(console_fmt)

    def get_filename(self, filename):
        self.filename = filename

    def save_log(self):
        """Save file to log."""
        if self.filename == "":
            return

        log_file_name = f"{self.filename}.log"
        log_file = QFileDialog.getSaveFileName(self, 'Save File', log_file_name)

        with open(log_file[0], 'w') as saved_log_file:
            log_contents = self.log_handler.log_file_buffer.getvalue()
            saved_log_file.write(log_contents)


class QTextEditLoggerHandler(logging.Handler):
    """Plain Text Edit Logger Handler."""

    def __init__(self, parent):
        """Construct from parent."""
        super().__init__()
        self.console = parent
        self.log_file_buffer = StringIO()
        self.log_file_buffer.write(f"{datetime.datetime.now()}")

    def emit(self, log):
        """Emit log to debug console."""
        msg: str = self.format(log)
        level: list[str] = msg.split(" ")

        # TODO USE MATCH

        if level[0] == "[DEBUG]":
            self.console.setTextColor(QColor('#000000'))

        elif level[0] == "[INFO]":
            self.console.setTextColor(QColor('#145A00'))

        elif level[0] == "[WARNING]":
            self.console.setTextColor(QColor('#FFDB19'))

        elif level[0] == "[ERROR]":
            self.console.setTextColor(QColor('#ED9017'))

        elif level[0] == "[CRITICAL]":
            self.console.setTextColor(QColor('#A40800'))

        self.log_file_buffer.write(msg + "\n")
        self.console.append(msg)


class GraphWindowDialog(QDialog):
    """Graph Window."""
    def __init__(self):
        """Init function."""
        super().__init__()

        loadUi(os.path.join(basedir, "UI", "GraphWindow.ui"), self)
        self.setWindowIcon(QIcon(os.path.join(basedir, "UI\\icon", "graphs.ico")))
        self.setWindowTitle("Graph")
        self.setWindowFlags(Qt.WindowStaysOnTopHint)

        # self.vlayout = QVBoxLayout()
        self.top_layout = self.findChild(QVBoxLayout, "topLayout")
        self.checkbox_layout = self.findChild(QGridLayout, "checkboxLayout")

        # Create Widgets
        self.doneButton = QPushButton("Done")
        self.deleteButton = QPushButton("Delete Graph")
        self.previewButton = QPushButton("Preview Graph Contents")
        self.nextButton = QPushButton("Create Graph")

        # Set button functionality
        self.doneButton.clicked.connect(self.close_graph)
        self.nextButton.clicked.connect(self.create_graph)
        self.deleteButton.clicked.connect(self.delete_graph)
        self.previewButton.clicked.connect(self.preview_graph)

        # Add widgets to screen
        self.top_layout.addWidget(self.doneButton)
        self.top_layout.addWidget(self.deleteButton)
        self.top_layout.addWidget(self.previewButton)
        self.top_layout.addWidget(self.nextButton)

        self.columns_list: list[Column] = []
        self.name_to_column: dict[str, Column] = {}

        self.graph_list: list[Graph] = []
        self.name_to_graph: dict[str, Graph] = {}

        self.checkboxes: list[QCheckBox] = []
        self.GraphListWidget = QListWidget()

        #self.GraphListWidget = self.findChild(QListWidget, "graphListWidget")

        # Data Structure Attribute list
        self.ds_attributes: list[any] = []

        self.ds_attributes.append(self.columns_list)
        self.ds_attributes.append(self.name_to_column)
        self.ds_attributes.append(self.graph_list)
        self.ds_attributes.append(self.name_to_graph)

        self.top_layout.addWidget(self.GraphListWidget)
        self.checkbox_fcc: int = 0
        self.checkbox_scc: int = 0
        #self.setLayout(self.vlayout)

    def get_ds_attributes(self) -> list[any]:
        """Return list of data structure attributes"""
        return self.ds_attributes

    def init_graph(self):
        """Init Graph"""
        column_list_len = len(self.columns_list)

        if column_list_len % 2 == 0:
            half_len_rounded = column_list_len / 2

        else:
            half_len_rounded = (column_list_len - 1) / 2

        for index, column in enumerate(self.columns_list):
            self.name_to_column[column.title] = column

            if index + 1 <= half_len_rounded:
                self.add_checkbox(column.title, "Left")

            else:
                self.add_checkbox(column.title, "Right")


    def add_checkbox(self, name, side):
        """Create Checkbox"""
        checkbox_name = QCheckBox(name)
        self.checkboxes.append(checkbox_name)

        if side == "Left":
            self.checkbox_layout.addWidget(checkbox_name, self.checkbox_fcc, 0)
            self.checkbox_fcc += 1

        elif side == "Right":
            self.checkbox_layout.addWidget(checkbox_name, self.checkbox_scc, 1)
            self.checkbox_scc += 1

        checkbox_name.setText(name)

    def create_graph(self):
        """Create Graph"""

        # List of selected checkboxes
        checked_list: list[QCheckBox] = []

        # Bool for if any checkboxes were checked
        some_checked: bool = False

        for checkbox in self.checkboxes:
            if checkbox.isChecked():
                checked_list.append(checkbox)
                checkbox.setChecked(False)
                some_checked = True

        if not some_checked:
            return

        graph: Graph = Graph(f"Graph {self.GraphListWidget.count() + 1}", [])
        self.name_to_graph[graph.title] = graph

        for checkbox in checked_list:
            column = self.name_to_column[checkbox.text()]
            graph.add_column(column)

        self.graph_list.append(graph)
        self.GraphListWidget.addItem(graph.title)

    def delete_graph(self):
        """Delete selected Graph"""
        # Item selected in GraphWidget
        selected_items_list: list[QListWidgetItem] = self.GraphListWidget.selectedItems()

        for item in selected_items_list:
            graph_title = item.text()
            graph = self.name_to_graph[graph_title]
            self.graph_list.remove(graph)
            del self.name_to_graph[graph_title]
            row = self.GraphListWidget.row(item)
            self.GraphListWidget.takeItem(row)

    def preview_graph(self):
        """Show Graph Preview."""
        selected_items_list: list[QListWidgetItem] = self.GraphListWidget.selectedItems()

        for item in selected_items_list:
            graph_title = item.text()
            graph = self.name_to_graph[graph_title]
            graph_preview = GraphWindowPreviewDialog(graph)
            graph_preview.show()
            graph_preview.exec()

    def close_graph(self):
        """Set Done to True."""
        self.close()


class GraphWindowPreviewDialog(QDialog):
    """Graph Window."""
    def __init__(self, graph):
        """Init function."""
        super().__init__()
        self.graph = graph

        loadUi(os.path.join(basedir, "UI", "PreviewWindow.ui"), self)
        self.setWindowIcon(QIcon(os.path.join(basedir, "UI\\icon", "graphs.ico")))
        self.setWindowTitle(f"{graph.title}")
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.resize(300, 200)

        self.vlayout = QVBoxLayout()

        self.GraphListWidget = QListWidget()

        self.vlayout.addWidget(self.GraphListWidget)
        self.setLayout(self.vlayout)

        for column in graph.columns:
            self.GraphListWidget.addItem(column.title)