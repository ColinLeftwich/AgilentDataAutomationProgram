import os
import re
import gc
from pathlib import Path
from time import time
import datetime as dt

import chardet
from xlwings import constants
import pandas as pd

import dpt
from agilent import Graph, NewAgilentFile, OldAgilentFile, Column


def new_agilent_cleanup(xlf: NewAgilentFile):
    """Clean up."""
    try:
        ic_number = xlf.ic_start_cell.number + 1
        models_start_cell = "C{}".format(ic_number)
        xlf.data_sheet.range(models_start_cell).expand('right').clear()
        xlf.ic_start_cell = None
        xlf.logger.info("Instrument Configuration Cleared")

    except Exception as error:
        xlf.logger.critical(f"Type: {type(error)}")
        xlf.logger.critical(f"{error}")

    try:
        cc_number = xlf.cc_start_cell.number + 1
        function_start_cell = "C{}".format(cc_number)
        xlf.data_sheet.range(function_start_cell).expand().clear()
        xlf.logger.info("Chanel Configuration Cleared")

    except Exception as error:
        xlf.logger.critical(f"Type: {type(error)}")
        xlf.logger.critical(f"{error}")

    if xlf.ri_start_cell is not None:
        try:
            relay_value_range = xlf.data_sheet.range(xlf.ri_start_cell.value).expand('down')
            relay_name = []
            ri_number = xlf.ri_start_cell.number
            deleted = 0

            range_test = range(len(relay_value_range))

            for index in range_test:
                cell = "{}:{}".format(ri_number + index - deleted, ri_number + index - deleted)
                relay_name.append(cell)
                deleted += 1

            for cell in relay_name:
                xlf.data_sheet.range(cell).delete()

            xlf.logger.info("Relay Information Columns Deleted")
            xlf.ri_start_cell = None
            xlf.cell_cache()

        except Exception as error:
            xlf.logger.critical(f"Type: {type(error)}")
            xlf.logger.critical(f"{error}")
            xlf.logger.exception(f"{error}")


def old_agilent_cleanup(xlf: OldAgilentFile):
    """Clean up."""
    try:
        ic_number = xlf.ic_start_cell.number
        models_start_cell = "C{}".format(ic_number)
        xlf.data_sheet.range(models_start_cell).expand('right').clear()
        xlf.logger.info("Instrument Configuration Cleared")

    except Exception as error:
        xlf.logger.critical(f"Type: {type(error)}")
        xlf.logger.critical(f"{error}")

    try:
        data_number = xlf.data_start_cell.number - 1
        insert_row = "{}:{}".format(data_number, data_number)
        xlf.data_sheet.range(insert_row).insert(shift='down')
        cc_number = xlf.cc_start_cell.number + 1
        function_start_cell = "C{}".format(cc_number)
        xlf.data_sheet.range(function_start_cell).expand().clear()
        xlf.logger.info("Chanel Configuration Cleared")
        xlf.cell_cache()

    except Exception as error:
        xlf.logger.critical(f"Type: {type(error)}")
        xlf.logger.critical(f"{error}")


def delete_time_columns(xlf: NewAgilentFile):
    """Delete timestamp columns."""
    data = xlf.data_sheet.range(xlf.data_start_cell.value).expand('right').value

    # Offset by number columns deleted
    deleted = 0

    try:
        for index, value in enumerate(data):
            string = value.split(" ")
            if string[1] == "(Sec)":
                # TODO Remove Regex
                time_ch_name = string[0]
                # timestamp = re.sub('(?<=\d)(?!\d)|(?<!\d)(?=\d)', ' ', string[0]).split(" ")
                if time_ch_name[0:3] == "Time":
                    letter = dpt.indexToColoum(index + 1 - deleted)
                    string = '{}:{}'.format(letter[0], letter[1])
                    xlf.data_sheet.range(string).delete()
                    deleted += 1

    except Exception as error:
        xlf.logger.critical(f"Type: {type(error)}")
        xlf.logger.critical(f"{error}")
        xlf.logger.exception(f"{error}")


def delete_alarm_columns(xlf: OldAgilentFile):
    """Delete timestamp columns."""
    data = xlf.data_sheet.range(xlf.data_start_cell.value).expand('right').value

    # Offset by number columns deleted
    deleted = 0

    try:
        for index, value in enumerate(data):
            string = value.split(" ")
            if string[0] == "Alarm":
                letter = dpt.indexToColoum(index + 1 - deleted)
                string = '{}:{}'.format(letter[0], letter[1])
                xlf.data_sheet.range(string).delete()
                deleted += 1

    except Exception as error:
        xlf.logger.critical(f"Type: {type(error)}")
        xlf.logger.critical(f"{error}")
        xlf.logger.exception(error)


def old_move_data(xlf: NewAgilentFile | OldAgilentFile):
    """Move data range to the top."""
    try:
        data_start_row: int = xlf.data_start_cell.number
        data_title_str: str = "{}{}".format('A', data_start_row - 1)
        data_str: str = "{}{}".format('C', data_start_row)

        data_buffer = xlf.data_sheet.range(data_str).options(pd.DataFrame, expand='table').value
        xlf.data_sheet.range(data_str).expand().clear()
        xlf.data_sheet.range('H1').value = data_buffer

        time_coloumn_buffer = xlf.data_sheet.range(xlf.time_start_cell.value).options(pd.DataFrame, dates=dt.date, expand='down').value
        xlf.data_sheet.range(xlf.time_start_cell.value).expand('down').clear()
        xlf.data_sheet.range('G1').value = time_coloumn_buffer
        xlf.data_sheet.range(xlf.time_start_cell.value).expand('down').clear()
        xlf.data_sheet.range('G1').clear()
        xlf.data_sheet.range(data_title_str).expand('right').clear()

        # Marks data_buffer for deletion
        del data_buffer
        del time_coloumn_buffer
        gc.collect()

        xlf.data_start_cell.update('H1')
        xlf.logger.info(f"Data moved to: {xlf.data_start_cell}")
        # xlf.range('F7').expand('down').number_format = 'mm.ss.0'

    except Exception as error:
        xlf.logger.critical(f"Type: {type(error)}")
        xlf.logger.critical(f"{error}")
        xlf.logger.exception(error)


def new_move_data(xlf: NewAgilentFile | OldAgilentFile):
    """Move data range to the top."""
    try:
        data_start_row: str = xlf.data_start_cell.number
        data_str: str = "{}{}".format('C', data_start_row)

        data_buffer: pd.DataFrame = xlf.data_sheet.range(data_str).options(pd.DataFrame, dates=dt.date, expand='table').value
        xlf.data_sheet.range(data_str).expand().clear()
        xlf.data_sheet.range('H1').value = data_buffer
        xlf.data_sheet.range('H1').expand().number_format = '0.00'

        scan_start_cell = "{}{}".format('B', xlf.data_start_cell.number)
        time_coloumn_buffer: pd.DataFrame = xlf.data_sheet.range(xlf.time_start_cell.value).options(pd.DataFrame, expand='down').value
        xlf.data_sheet.range(xlf.time_start_cell.value).expand('down').clear()
        xlf.data_sheet.range('G1').value = time_coloumn_buffer
        xlf.data_sheet.range(scan_start_cell).expand('down').clear()
        xlf.data_sheet.range('G1').clear()

        # Marks data_buffer for deletion
        del time_coloumn_buffer
        del data_buffer
        gc.collect()

        xlf.data_start_cell.update('H1')
        xlf.logger.info(f"Data Moved to: {xlf.data_start_cell}")

        xlf.data_sheet.range('G2').expand('down').number_format = 'm/dd/yyyy h:mm:ss AM/PM'
        xlf.data_sheet.autofit()

    except Exception as error:
        xlf.logger.critical(f"Type: {type(error)}")
        xlf.logger.critical(f"{error}")
        xlf.logger.exception(error)


def create_graphs(xlf: NewAgilentFile | OldAgilentFile, GraphWindow, unit_index: int):
    """Create graphs."""
    xlf.create_graph_sheet()

    ch_name_range = xlf.data_sheet.range(xlf.ch_name_start_cell.value).expand('down')

    ch_number_range = xlf.data_sheet.range(xlf.ch_number_start_cell.value).expand('down')

    data_range = xlf.data_sheet.range(xlf.data_start_cell.value).expand('right')

    try:
        for channel_title, start_cell, channel_number, in zip(ch_name_range, data_range, ch_number_range):
            GraphWindow.columns_list.append(Column(channel_title.value, start_cell.address, channel_number.value))

    except Exception as error:
        xlf.logger.critical(f"Type: {type(error)}")
        xlf.logger.critical(f"{error}")

    try:
        xlf.logger.info("Showing Graph")
        GraphWindow.init_graph()
        GraphWindow.show()
        GraphWindow.exec()

    except Exception as error:
        xlf.logger.critical(f"Type: {type(error)}")
        xlf.logger.critical(f"{error}")

    # graph position offset
    graph_pos_offset: int = 0

    # cell position offset
    cell_pos_offset: int = 0

    i = 0

    for graph in GraphWindow.graph_list:
        data_range_str: str = ""
        title_list: list[str] = []
        table_offset: int = 0
        graph_offset_counter: int = 0

        for index, column in enumerate(graph.columns):
            try:
                cell_point = xlf.graph_sheet.range((6 + cell_pos_offset, 3))
                cell_point.value = column.channel_number
                merge_range = xlf.graph_sheet.range((6 + cell_pos_offset, 3), (6 + cell_pos_offset, 5))
                merge_range.merge()
                center_content(merge_range)

                cell_point = xlf.graph_sheet.range((7 + cell_pos_offset, 3))
                cell_point.value = column.title
                merge_range = xlf.graph_sheet.range((7 + cell_pos_offset, 3), (7 + cell_pos_offset, 5))
                merge_range.merge()
                center_content(merge_range)

            except Exception as error:
                xlf.logger.critical(f"Type: {type(error)}")
                xlf.logger.critical(f"{error}")

            try:
                calc_range = xlf.graph_sheet.range((8 + cell_pos_offset, 3), (8 + cell_pos_offset, 5))
                calc_names = {
                    0: "MINIMUM",
                    1: "MAX",
                    2: "AVERAGE",
                }

                for cell_index, cell in enumerate(calc_range):
                    cell.value = calc_names.get(cell_index)
                    center_content(cell)

            except Exception as error:
                xlf.logger.critical(f"Type: {type(error)}")
                xlf.logger.critical(f"{error}")

            try:
                operation_range = xlf.graph_sheet.range((9 + cell_pos_offset, 3), (9 + cell_pos_offset, 5))
                operators_options = {
                    0: "min",
                    1: "max",
                    2: "average",
                }

                data_range = xlf.data_sheet.range(column.start_cell).expand('down')

                for op_index, cell in enumerate(operation_range):
                    operation = operators_options.get(op_index)
                    formula = "={}('{}'!{})".format(operation, xlf.workbook.sheets[0].name, data_range.address)
                    cal_range_value = xlf.graph_sheet.range((9 + cell_pos_offset, 3 + op_index))
                    cal_range_value.value = formula
                    center_content(cal_range_value)

            except Exception as error:
                xlf.logger.critical(f"Type: {type(error)}")
                xlf.logger.critical(f"{error}")
                xlf.logger.exception(f"{error}")

            data_range = xlf.data_sheet.range(column.start_cell).expand('down')
            data_range_str = data_range_str + f"{data_range.address}, "

            ch_title_range = xlf.data_sheet.range(column.start_cell)
            title_list.append(ch_title_range.address)

            if 1 <= index <= 3:
                table_offset = table_offset - 5

            elif index >= 4:
                graph_offset_counter += 1

            cell_pos_offset += 5
            i += 1

        try:
            time_range_str = "{}{}".format('G', xlf.data_start_cell.number)
            time_range = xlf.data_sheet.range(time_range_str).expand('down')
            data_range_str = data_range_str[:-2]
            graph_range_str = "{}, {}".format(time_range.address, data_range_str)

            graph_range = xlf.data_sheet.range(graph_range_str)
            graph_top = 75 + graph_pos_offset

            chart = xlf.graph_sheet.charts.add(left=288, top=graph_top, width=1200, height=308)
            chart.set_source_data(graph_range)
            chart.chart_type = 'xy_scatter_lines_no_markers'

            xlvalue = 1
            ylvalue = 2
            xlprimary = 1

            chart.api[1].Axes(xlvalue,xlprimary).HasTitle = True
            chart.api[1].Axes(xlvalue,xlprimary).AxisTitle.Text = "Time (Sec)"
            chart.api[1].Axes(ylvalue,xlprimary).HasTitle = True 
            unit_list = []

            for title in title_list:
                title_name = xlf.data_sheet.range(title)
                title_name_list = title_name.value.split(" ")

                unit = title_name_list[unit_index]

                if unit not in unit_list:
                    unit_list.append(unit)

            y_axis_title: str = ""

            for unit in unit_list:
                y_axis_title = y_axis_title + f"{unit} "

            chart.api[1].Axes(ylvalue,xlprimary).AxisTitle.Text = f"{y_axis_title}"

            graph_pos_offset += 360
            cell_pos_offset += 19 + table_offset
            graph_pos_offset = graph_pos_offset + (75 * graph_offset_counter)

        except Exception as error:
            xlf.logger.critical(f"Type: {type(error)}")
            xlf.logger.critical(f"{error}")
            xlf.logger.exception(f"{error}")

    try:
        xlf.workbook.sheets[1].autofit('c')

    except Exception as error:
        xlf.logger.critical(f"Type: {type(error)}")
        xlf.logger.critical(f"{error}")
        xlf.logger.exception(f"{error}")


def center_content(cell):
    """Centers a cell."""
    cell.api.HorizontalAlignment = constants.HAlign.xlHAlignCenter


def format_data(xlf):
    """Round 2 decimal places."""
    data_number = xlf.data_start_cell.number + 1
    xlf.data_sheet.range("C{}".format(data_number)).expand().number_format = '#.00'


def create_min_max_sheet(xlf: NewAgilentFile | OldAgilentFile, GraphWindow):
    """Create Min Max Sheet"""
    xlf.create_min_max_sheet()

    cell_pos_offset: int = 0

    i: int = 0

    for column in GraphWindow.columns_list:
        try:
            cell_point = xlf.graph_sheet.range((6 + cell_pos_offset, 3))
            cell_point.value = column.channel_number
            merge_range = xlf.graph_sheet.range((6 + cell_pos_offset, 3), (6 + cell_pos_offset, 5))
            merge_range.merge()
            center_content(merge_range)

            cell_point = xlf.graph_sheet.range((7 + cell_pos_offset, 3))
            cell_point.value = column.title
            merge_range = xlf.graph_sheet.range((7 + cell_pos_offset, 3), (7 + cell_pos_offset, 5))
            merge_range.merge()
            center_content(merge_range)

        except Exception as error:
            xlf.logger.critical(f"Type: {type(error)}")
            xlf.logger.critical(f"{error}")
            xlf.logger.exception(f"{error}")

        try:
            calc_range = xlf.graph_sheet.range((8 + cell_pos_offset, 3), (8 + cell_pos_offset, 5))
            calc_names = {
                0: "MINIMUM",
                1: "MAX",
                2: "AVERAGE",
            }

            for cell_index, cell in enumerate(calc_range):
                cell.value = calc_names.get(cell_index)
                center_content(cell)

        except Exception as error:
            xlf.logger.critical(f"Type: {type(error)}")
            xlf.logger.critical(f"{error}")

        try:
            operation_range = xlf.graph_sheet.range((9 + cell_pos_offset, 3), (9 + cell_pos_offset, 5))
            operators_options = {
                0: "min",
                1: "max",
                2: "average",
            }

            data_range = xlf.data_sheet.range(column.start_cell).expand('down')

            for op_index, cell in enumerate(operation_range):
                operation = operators_options.get(op_index)
                formula = "={}('{}'!{})".format(operation, xlf.workbook.sheets[0].name, data_range.address)
                cal_range_value = xlf.graph_sheet.range((9 + cell_pos_offset, 3 + op_index))
                cal_range_value.value = formula
                center_content(cal_range_value)

        except Exception as error:
            xlf.logger.critical(f"Type: {type(error)}")
            xlf.logger.critical(f"{error}")

        cell_pos_offset += 5
        i += 1


def open_file(input_file, filename, logger):
    """Create file and return Agilent data object."""
    parent_dir = Path(input_file).parent
    csv_working_file = parent_dir.joinpath(filename + ".csv")

    if os.path.exists(csv_working_file):
        try:
            logger.info("found file with same name")
            os.remove(csv_working_file)
            logger.info("deleting")
            # TODO ADD CANCEL

        except Exception as error:
            logger.critical(f"Type: {type(error)}")
            logger.critical(f"{error}")

    with open(input_file, 'rb') as data_file:
        file_metadata = chardet.detect(data_file.read())
        encoding = file_metadata['encoding']
        logger.info(f"Encoding: {encoding}")

        if encoding == "UTF-16":
            logger.info(f"Creating file: {csv_working_file}")

            try:
                with open(input_file, 'rb') as source_file:
                    with open(csv_working_file, 'w+b') as csv_formatted_file:
                        contents = source_file.read()
                        csv_formatted_file.write(contents.decode('utf-16').encode('utf-8'))

                # read_csv_file = pd.read_csv(csv_working_file, low_memory=False)
                # read_csv_file.to_excel(excel_working_file, index=None, header=False)
                # os.remove(csv_working_file)

                return OldAgilentFile(csv_working_file, logger)

            except Exception as error:
                logger.critical(f"Type: {type(error)}")
                logger.critical(f"{error}")

        if encoding == "UTF-8-SIG":
            logger.info(f"Creating file: {csv_working_file}")

            try:
                with open(input_file, 'rb') as source_file:
                    with open(csv_working_file, 'w+b') as csv_formatted_file:
                        contents = source_file.read()
                        csv_formatted_file.write(contents)

                # read_csv_file = pd.read_csv(csv_working_file, low_memory=False)
                # read_csv_file.to_excel(excel_working_file, index=None, header=False)
                # os.remove(csv_working_file)

                return NewAgilentFile(csv_working_file, logger)

            except Exception as error:
                logger.critical(f"Type: {type(error)}")
                logger.critical(f"{error}")
                logger.exception(error)


def run(input_file, filename, logger, GraphWindow) -> bool:
    """Run formatting."""
    xlf = open_file(input_file, filename, logger)

    if isinstance(xlf, NewAgilentFile):
        new_agilent_cleanup(xlf)
        delete_time_columns(xlf)
        new_move_data(xlf)
        create_graphs(xlf, GraphWindow, 1)
        create_min_max_sheet(xlf, GraphWindow)
        return True

    elif isinstance(xlf, OldAgilentFile):
        old_agilent_cleanup(xlf)
        delete_alarm_columns(xlf)
        old_move_data(xlf)
        create_graphs(xlf, GraphWindow, -1)
        create_min_max_sheet(xlf, GraphWindow)
        return True

    else:
        return False