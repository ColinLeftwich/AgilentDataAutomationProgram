import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication

from gui import MainWindow


def main():
    """create adap instance."""
    if hasattr(Qt, 'AA_EnableHighDpiScaling'):
        QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True)
    if hasattr(Qt, 'AA_UseHighDpiPixmaps'):
        QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps, True)

    app = QApplication(sys.argv)
    mw = MainWindow()
    app.exec_()


if __name__ == "__main__":
    main()
