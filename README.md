# Keysight DAQ - Agilent Data Automation Program

## 1. Intro

ADAP is a open source python excel program for automating the graphing of tabular data generatated from a Keysight Benchtop DAQ.

DAQ or Data Aquisition Systems is any system that samples then converts signals thats measure real world data into digital data. 

Keysight Benchtop DAQ's are a form of DAQ/data logger that proccess then and records data over time. They are generally used when wanting to analyzes how a phycial system changes over time. When the data is done being collected it will create a ouput file ussualy in the form of a csv. 

ADAP will read these csv files and automate the proccess of graphing this data in excel. 

## 2. Dependencies

ADAP using PyQt5 for the front end and interfaces with excel using the xlwings and the pywin32 libary.

### 2.1 Build dependencies

* PyQt5 5.15 
* xlwings
* pywin32

## 3. Supported Keysight Benchtop DAQ's

These are the supported list of DAQ file formats the work with ADAP


|       Instrument      |   Supported   |
|-----------------------|---------------|
| Keysight DAQ973A DAQ  |  Yes &check;  |
| Keysight DAQ970A DAQ  |  Yes &check;  |
